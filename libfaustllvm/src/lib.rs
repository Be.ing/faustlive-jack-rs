use std::rc::Rc;
use std::ffi::CString;
use libfaustllvm_sys::*;


struct FaustLLVMFactoryPointer {
    ptr: *mut LlvmDspFactoryC
}

impl Drop for FaustLLVMFactoryPointer {
    fn drop(&mut self) {
        unsafe {
            deleteCDSPFactory(self.ptr);
        }
    }
}

pub struct FaustLLVMFactory {
    ctx: Rc<FaustLLVMFactoryPointer>
}

unsafe impl Send for FaustLLVMFactory {}
unsafe impl Sync for FaustLLVMFactory {}

impl FaustLLVMFactory {
    pub fn from_file(file: &str) -> Result<Self, Box<dyn std::error::Error>> {
        // Make sure we are running in multi thread safe mode
        assert_eq!(unsafe { startMTDSPFactories() }, true);

        let c_file = CString::new(file).unwrap();
        let ptr = create_dsp_factory(&c_file)? as *mut LlvmDspFactoryC;

        Ok(Self { ctx: Rc::new(FaustLLVMFactoryPointer { ptr }) })

    }

    pub fn as_mut_ptr(&self) -> *mut LlvmDspFactoryC {
        self.ctx.ptr
    }
    pub fn as_ptr(&self) -> *const LlvmDspFactoryC {
        self.ctx.ptr
    }
}

impl Clone for FaustLLVMFactory {
    fn clone(&self) -> FaustLLVMFactory {
        FaustLLVMFactory {
            ctx: self.ctx.clone()
        }
    }
}

impl Drop for FaustLLVMFactory {
    fn drop(&mut self) {
    }
}

pub struct FaustLLVMInstance {
    llvm_dsp_c: *const LlvmDspC,
    _factory: FaustLLVMFactory
}

unsafe impl Send for FaustLLVMInstance {}

impl From<FaustLLVMFactory> for FaustLLVMInstance {
    fn from(factory: FaustLLVMFactory) -> Self {
        let llvm_dsp_c = unsafe {
            createCDSPInstance(factory.as_mut_ptr())
        };

        Self { llvm_dsp_c, _factory: factory }
    }

}


impl Drop for FaustLLVMInstance {
    fn drop(&mut self) {
        unsafe {
            //println!("Drop instance");
            deleteCDSPInstance(self.llvm_dsp_c);
        }
    }
}

impl FaustLLVMInstance {
    pub fn init(&mut self, sample_rate: usize)  {
        unsafe {
            initCDSPInstance(self.llvm_dsp_c, sample_rate as i32);
        }
    }

    pub fn outputs(&self) -> usize {
        unsafe {
            getNumOutputsCDSPInstance(self.llvm_dsp_c) as usize
        }
    }

    pub fn inputs(&self) -> usize {
        unsafe {
            getNumInputsCDSPInstance(self.llvm_dsp_c) as usize
        }
    }

    pub fn compute(&self, count: usize, input: *const *const f32,  output: *mut *mut f32) {
        unsafe {
            computeCDSPInstance(self.llvm_dsp_c, count as i32, input, output);
        }
    }
}

pub struct FaustBuffer {
    pub buffer: Vec<Vec<f32>>,
    _buffer_c_repr: Vec<*mut f32>
}

impl FaustBuffer {
    pub fn new(channels: usize, count: usize) -> Self {
        let mut buffer = vec![vec![0_f32; count]; channels];
        let mut _buffer_c_repr = vec![0_i32 as *mut f32; channels];  
        for i in 0..channels {
            _buffer_c_repr[i] = buffer[i].as_mut_slice().as_mut_ptr();
        }

        Self {
            buffer,
            _buffer_c_repr
        }

    }

    pub fn as_mut_ptr(&mut self) -> *mut *mut f32 {
        self._buffer_c_repr.as_mut_slice().as_mut_ptr()
    }
    pub fn as_const_ptr(&self) -> *const *const f32 {
        self._buffer_c_repr.as_slice().as_ptr() as *const *const f32
    }
}
