import("stdfaust.lib");
import("oscillator.lib");


BPM = 128;
beat = ba.beat(BPM);
pulseoff(bpm, n) = _ : de.delay(ba.tempo(bpm*n) + 1, ba.tempo(bpm*n)) : _;
beatoff(n) = beat : pulseoff(BPM, n);
subbeat2secs(n) = tempo2secs(BPM*n);


tempo2secs(t) = ba.tempo(t) : ba.samp2sec;
kick = os.osc(80 + 220 * en.ar(0.0005, subbeat2secs(32), beat)) * en.ar(0.0005, subbeat2secs(2), beat) <: _,ef.cubicnl(0.001, 1) * 0.8 :> _ * 0.8;
hihat = no.noise * en.ar(0.5, subbeat2secs(4+os.osc(2)*2), ba.beat(BPM * 4) : pulseoff(BPM * 2, 5)) * 0.05;
process = kick + hihat <: _,_;
