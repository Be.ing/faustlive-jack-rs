use core::ffi::c_void;
use std::error::Error;
use std::ffi::CStr;
use std::ffi::CString;
use std::fmt;
use std::os::raw::c_char;
use std::os::raw::c_int;

#[derive(Debug)]
pub struct FaustError {
    err_msg: String,
}

impl FaustError {
    pub fn new(err_msg: String) -> Self {
        FaustError { err_msg }
    }
}

impl fmt::Display for FaustError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.err_msg)
    }
}

impl Error for FaustError {
    fn description(&self) -> &str {
        &self.err_msg
    }
}


#[repr(C)]
/// C Pointer for the llvm_dsp_factory
pub struct LlvmDspFactoryC {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*const u8, core::marker::PhantomPinned)>,
}


#[repr(C)]
/// C Pointer for the llvm_dsp
pub struct LlvmDspC {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*const u8, core::marker::PhantomPinned)>,
}

// Can probably cut back some of these at some point, there's only a few we need really
#[link(name = "faust")]
extern "C" {
    pub fn createCDSPFactoryFromFile(
        filename: *const c_char,
        argc: c_int,
        argv: *const *const c_char,
        target: *const c_char,
        error_msg: *const c_char,
        opt_level: c_int,
    ) -> *const LlvmDspFactoryC;
    pub fn deleteCDSPFactory(llvm_dsp_factory: *const LlvmDspFactoryC) -> bool;
    pub fn startMTDSPFactories() -> bool;
    pub fn stopMTDSPFactories();

    pub fn initCDSPInstance(llvm_dsp: *const LlvmDspC, sample_rate: c_int);
    pub fn deleteCDSPInstance(llvm_dsp: *const LlvmDspC);
    pub fn createCDSPInstance(llvm_dsp_factory: *const LlvmDspFactoryC) -> *const LlvmDspC;
    // This is the core function we need to put in the jack callback
    pub fn computeCDSPInstance(
        llvm_dsp: *const LlvmDspC,
        count: c_int,
        input: *const *const f32,
        output: *mut *mut f32,
    );
    pub fn getNumOutputsCDSPInstance(llvm_dsp: *const LlvmDspC) -> c_int;
    pub fn getNumInputsCDSPInstance(llvm_dsp: *const LlvmDspC) -> c_int;
    pub fn freeCMemory(ptr: *const c_void);
}
pub fn create_dsp_factory(filename: &CStr) -> Result<*const LlvmDspFactoryC, Box<dyn Error>> {
    let target = CString::new("").unwrap();
    let mut error_msg_buffer = vec![0i8; 4096];
    let dsp_factory = unsafe {
        createCDSPFactoryFromFile(
            filename.as_ptr(),
            0,
            std::ptr::null(),
            target.as_ptr(),
            error_msg_buffer.as_mut_ptr(),
            0,
        )
    };
    if dsp_factory.is_null() {
        return unsafe {
            Err(Box::new(FaustError::new(String::from(
                CStr::from_ptr(error_msg_buffer.as_mut_ptr()).to_str()?,
            ))))
        };
    }
    Ok(dsp_factory)
}
